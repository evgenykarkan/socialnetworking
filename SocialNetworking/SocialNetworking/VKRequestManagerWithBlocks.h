//
//  VKRequestManagerWithBlocks.h
//  SocialNetworking
//
//  Created by Bondarenko Alexander on 10/21/13.
//  Copyright (c) 2013 Sigma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VKRequestManager.h"
#import "VKRequestWithBlock.h"

@interface VKRequestManagerWithBlocks : VKRequestManager

- (void)postData:(NSData *)data toURL:(NSString *)url withComplitionBlock:(VKRequestComplitionBlock)block;

@end
