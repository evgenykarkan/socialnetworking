//
//  VKRequestManagerWithBlocks.m
//  SocialNetworking
//
//  Created by Bondarenko Alexander on 10/21/13.
//  Copyright (c) 2013 Sigma. All rights reserved.
//

#import "VKRequestManagerWithBlocks.h"
#import "NSData+toBase64.h"

@implementation VKRequestManagerWithBlocks

- (VKRequest *)configureRequestMethod:(NSString *)methodName
                              options:(NSDictionary *)options
                             selector:(SEL)selector
{
    if (nil != self.user) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
        options = [self performSelector:@selector(addAccessTokenKey:) withObject:options];
#pragma clang diagnostic pop
    }
    
    VKRequestWithBlock *request = [[VKRequestWithBlock alloc]
                                   initWithMethod:methodName
                                   options:options];
    
    request.signature = NSStringFromSelector(selector);
    request.offlineMode = self.offlineMode;
    request.delegate = self.delegate;
    
    return request;
}

- (void)postData:(NSData *)data toURL:(NSString *)url withComplitionBlock:(VKRequestComplitionBlock)block
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL
                                                                        URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request setHTTPMethod:@"POST"];
    
    [request addValue:@"8bit" forHTTPHeaderField:@"Content-Transfer-Encoding"];
    
    CFUUIDRef uuid = CFUUIDCreate(nil);
    NSString *uuidString = (NSString*)CFBridgingRelease(CFUUIDCreateString(nil, uuid));
    CFRelease(uuid);
    NSString *stringBoundary = [NSString stringWithFormat:@"0xKhTmLbOuNdArY-%@",uuidString];
    NSString *endItemBoundary = [NSString stringWithFormat:@"\r\n--%@\r\n",stringBoundary];
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data;  boundary=%@", stringBoundary];
    
    [request setValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n",stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"photo\"; filename=\"photo.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: image/jpg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:data];
    [body appendData:[[NSString stringWithFormat:@"%@",endItemBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];

    NSError *error = nil;

    id result = [self responseWithJsonData:[NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error]];
    
    if (block)
    {
        block(result, error);
    }
}

- (id)responseWithJsonData:(NSData *)jsonData
{
    return [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
}

- (NSData *)jsonDataWithDictionary:(NSDictionary *)dictionary
{
    NSError *error = nil;
    NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
    
    return data;
}

@end
