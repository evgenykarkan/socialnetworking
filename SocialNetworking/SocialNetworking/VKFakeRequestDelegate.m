//
//  VKFakeRequestDelegate.m
//  SocialNetworking
//
//  Created by Bondarenko Alexander on 10/21/13.
//  Copyright (c) 2013 Sigma. All rights reserved.
//

#import "VKFakeRequestDelegate.h"

@implementation VKFakeRequestDelegate

- (void)callBlockWithResponse:(id)response withError:(NSError *)error
{
    if (self.complitionBlock)
    {
        self.complitionBlock(response,error);
    }
}

#pragma mark VKRequestDelegate

- (void)VKRequest:(VKRequest *)request response:(id)response
{
    if ([self.originDelegate respondsToSelector:@selector(VKRequest:response:)])
    {
        [self.originDelegate VKRequest:request response:response];
    }
    
    [self callBlockWithResponse:response withError:nil];
}

- (void)VKRequest:(VKRequest *)request connectionErrorOccured:(NSError *)error
{
    if ([self.originDelegate respondsToSelector:@selector(VKRequest:connectionErrorOccured:)])
    {
        [self.originDelegate VKRequest:request connectionErrorOccured:error];
    }
    
    [self callBlockWithResponse:nil withError:error];
}

- (void)VKRequest:(VKRequest *)request parsingErrorOccured:(NSError *)error
{
    if ([self.originDelegate respondsToSelector:@selector(VKRequest:parsingErrorOccured:)])
    {
        [self.originDelegate VKRequest:request parsingErrorOccured:error];
    }
}

- (void)VKRequest:(VKRequest *)request responseErrorOccured:(id)error
{
    if ([self.originDelegate respondsToSelector:@selector(VKRequest:responseErrorOccured:)])
    {
        [self.originDelegate VKRequest:request responseErrorOccured:error];
    }
}

- (void)VKRequest:(VKRequest *)request
       captchaSid:(NSString *)captchaSid
     captchaImage:(NSString *)captchaImage
{
    if ([self.originDelegate respondsToSelector:@selector(VKRequest:captchaSid:captchaImage:)])
    {
        [self.originDelegate VKRequest:request captchaSid:captchaSid captchaImage:captchaImage];
    }
}

- (void)VKRequest:(VKRequest *)request
       totalBytes:(NSUInteger)totalBytes
  downloadedBytes:(NSUInteger)downloadedBytes
{
    if ([self.originDelegate respondsToSelector:@selector(VKRequest:totalBytes:downloadedBytes:)])
    {
        [self.originDelegate VKRequest:request totalBytes:totalBytes downloadedBytes:downloadedBytes];
    }
}

- (void)VKRequest:(VKRequest *)request
       totalBytes:(NSUInteger)totalBytes
    uploadedBytes:(NSUInteger)uploadedBytes
{
    if ([self.originDelegate respondsToSelector:@selector(VKRequest:totalBytes:uploadedBytes:)])
    {
        [self.originDelegate VKRequest:request totalBytes:totalBytes uploadedBytes:uploadedBytes];
    }
}

@end
