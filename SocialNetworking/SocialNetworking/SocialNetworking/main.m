//
//  main.m
//  SocialNetworking
//
//  Created by Bondarenko Alexander on 10/15/13.
//  Copyright (c) 2013 Sigma. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SUAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SUAppDelegate class]));
    }
}
