//
//  SUAppDelegate.m
//  SocialNetworking
//
//  Created by Bondarenko Alexander on 10/15/13.
//  Copyright (c) 2013 Sigma. All rights reserved.
//

#import "SUAppDelegate.h"
#import "TMAPIClient.h"

static NSString * const kClientID = @"1023265671770.apps.googleusercontent.com";

@implementation SUAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [GPPSignIn sharedInstance].clientID = kClientID;
    
    [GPPDeepLink setDelegate:self];
    [GPPDeepLink readDeepLinkAfterInstall];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if ([[TMAPIClient sharedInstance] handleOpenURL:url] ||
        [GPPURLHandler handleURL:url
               sourceApplication:sourceApplication
                      annotation:annotation])
    {
        return YES;
    }
    
    return NO;
}

#pragma mark - GPPDeepLinkDelegate

- (void)didReceiveDeepLink:(GPPDeepLink *)deepLink
{
    // An example to handle the deep link data.
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Deep-link Data"
                          message:[deepLink deepLinkID]
                          delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
    [alert show];
}


@end
