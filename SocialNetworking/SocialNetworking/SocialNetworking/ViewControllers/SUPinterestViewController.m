//
//  SUPinterestViewController.m
//  SocialNetworking
//
//  Created by Bondarenko Alexander on 10/17/13.
//  Copyright (c) 2013 Sigma. All rights reserved.
//

#import "SUPinterestViewController.h"
#import <Pinterest/Pinterest.h>
#import <Pinterest/Pinterest.h>

@interface SUPinterestViewController ()

@property (nonatomic, strong) Pinterest *pinterest;

@end

@implementation SUPinterestViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.pinterest = [[Pinterest alloc] initWithClientId:@"1433575"];
}

- (IBAction)postAction:(id)sender
{
    if ([self.pinterest canPinWithSDK])
    {
        [self.pinterest createPinWithImageURL:[NSURL URLWithString:@"http://upload.wikimedia.org/wikipedia/commons/thumb/0/0e/Felis_silvestris_silvestris.jpg/265px-Felis_silvestris_silvestris.jpg"]
                                    sourceURL:nil
                                  description:@"test text"];
    }
}
@end
