//
//  SUFacebookViewController.m
//  SocialNetworking
//
//  Created by Bondarenko Alexander on 10/17/13.
//  Copyright (c) 2013 Sigma. All rights reserved.
//

#import "SUFacebookViewController.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@interface SUFacebookViewController ()

@property (nonatomic, strong) SLComposeViewController *composeViewController;

@end

@implementation SUFacebookViewController

- (IBAction)postText:(id)sender
{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        self.composeViewController = [[SLComposeViewController alloc] init];
        self.composeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [self.composeViewController setInitialText:@"test text"];
        [self presentViewController:self.composeViewController animated:YES completion:nil];
    }
}

- (IBAction)postImage:(id)sender
{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        self.composeViewController = [[SLComposeViewController alloc] init];
        self.composeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [self.composeViewController addImage:[UIImage imageNamed:@"image.png"]];
        [self presentViewController:self.composeViewController animated:YES completion:nil];
    }
}

@end
